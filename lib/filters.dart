import 'dart:ui';

class Filters {
  
  static const invert = ColorFilter.matrix(<double>[
    -1,  0,  0, 0, 255, 
    0, -1,  0, 0, 255,
    0,  0, -1, 0, 255,
    0,  0,  0, 1,   0,
  ]);

  static const greyscale = ColorFilter.matrix(<double>[
    0.2126, 0.7152, 0.0722, 0, 0,
    0.2126, 0.7152, 0.0722, 0, 0,
    0.2126, 0.7152, 0.0722, 0, 0,
    0,      0,      0,      1, 0,
  ]);

  static const List<num> tmpfilter = [
    0, -1, 0,
    -1, 4, -1,
    0, -1, 0,
  ];
}
